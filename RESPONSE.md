# ICI MES CHOIX TECHNIQUES

Je choisis de travailler avec le modèle vue en cours et lors du TP1 (Clean Architecture).

Problèmes de "books" restant dans le squelette du projet :
    \src\infrastructure\database\repositories\task\index.ts -> './book.repository' au lieu de './task.repository'
    \src\contexts\task\infrastructure\controller\index.ts -> './book.controller' au lieu de './task.controller'
    \src\contexts\task\use-cases\get-tasks\index.ts -> './get-book.use-case' au lieu de './get-tasks.use-case'

Utilisation de Prisma pour le schéma

Pour la question 5 visant à proposer de quoi récupérer les tâches ordonnées par leur priorité, j'ai modifié le getTasks afin que les tâches soient triées.