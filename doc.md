  URL: http://localhost:3002/api/task
  VERBE: GET
  -------
  URL: http://localhost:3002/api/task/{id}
  VERBE: GET
  -------
  URL: http://localhost:3002/api/task
  VERBE: POST
  ENTREE BODY: { title: string!, description: string?, status: string!, priority: number! }
  -------
  URL: http://localhost:3002/api/task/{id}
  VERBE: PUT
  ENTREE BODY: { title: string?, description: string?, status: string?, priority: number? }
  -------
  URL: http://localhost:3002/api/task/{id}
  VERBE: DELETE