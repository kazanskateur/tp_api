import { Tag, TagCreate } from '../../domains/types'
import { ITagRepository, TagRaw } from '../../infrastructure'

export class AddTagUseCase {
  constructor(private tagRepository: ITagRepository) {}

  async execute(tag: TagCreate): Promise<TagRaw> {
    return this.tagRepository.addTag(tag)
  }
}
