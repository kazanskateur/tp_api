import { Tag } from '../../domains/types'
import { ITagRepository, TagRaw } from '../../infrastructure'

export class GetTagsUseCase {
  constructor(private taskRepository: ITagRepository) { }

  async execute(): Promise<TagRaw[]> {
    return await this.taskRepository.getTags()
  }
}
