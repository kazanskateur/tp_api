import { Router } from 'express'
import { TagController } from './controller'

export function tagRoutes(controller: TagController): Router {
  const router = Router()
  router.post('/', controller.addTag.bind(controller))
  router.get('/', controller.getTags.bind(controller))
  /*
  router.get('/:id', controller.getTask.bind(controller))
  router.put('/:id', controller.updateTask.bind(controller))
  router.delete('/:id', controller.deleteTask.bind(controller))*/
    
  return router
}
