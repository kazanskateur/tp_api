import { Tag, TagCreate } from '../domains/types'
export * from '../domains/errors';

export type TagRaw = Tag


export interface ITagRepository {
  addTag(tag: TagCreate): Promise<TagRaw>
  getTags(): Promise<TagRaw[]>
  /*addTask(task: TaskCreate): Promise<TaskRaw>
  getTask(id: string): Promise<TaskRaw | null>
  updateTask(book: TaskUpdate): Promise<TaskRaw>
  deleteTask(id: string): Promise<void>*/
}