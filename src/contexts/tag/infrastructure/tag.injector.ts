import { Router } from 'express'
import { TagRepository, RelationalDatabase } from '../../../infrastructure/database'
import { tagRoutes } from './tag.routes'
import { TagController } from './controller'
import { AddTagUseCase, GetTagsUseCase,/* GetTaskUseCase, UpdateTaskUseCase, DeleteTaskUseCase*/ } from '../use-cases'

export type TagExternalDependencies = {
  database: RelationalDatabase
}

export const tagInjector = (externalDependencies: TagExternalDependencies): Router => {
  const tagRepository = new TagRepository(externalDependencies.database)

  
  const addTagUseCase = new AddTagUseCase(tagRepository)
  const getTagsUseCase = new GetTagsUseCase(tagRepository)
  /*
  const getTaskUseCase = new GetTaskUseCase(taskRepository)
  const updateTaskUseCase = new UpdateTaskUseCase(taskRepository)
  const deleteTaskUseCase = new DeleteTaskUseCase(taskRepository)*/

  const tagController = new TagController(
    addTagUseCase,
    getTagsUseCase,
    /*
    getTaskUseCase,
    updateTaskUseCase,
    deleteTaskUseCase*/
  )

  return tagRoutes(tagController)
}
