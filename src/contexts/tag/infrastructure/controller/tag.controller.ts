import { Request, Response } from 'express'
import { AddTagUseCase, GetTagsUseCase,  /*GetTaskUseCase, UpdateTaskUseCase, DeleteTaskUseCase*/ } from '../../use-cases'
import { TaskNotFoundError } from '../../domains'
import { internal, notFound } from '../../../../infrastructure'
import { ValidationError, validate } from 'jsonschema'
import { ValidatorResult } from 'jsonschema'

const tagCreateSchema = {
  id: "/Tag",
  type: "object",
  properties: {
    title: {
      type: "string"
    },
  },
  required: ["title"]
}

export class TagController {
  constructor(
    private readonly addTagUseCase: AddTagUseCase,
    private readonly getTagsUseCase: GetTagsUseCase,
    /*
    private readonly getTaskUseCase: GetTaskUseCase,
    private readonly updateTaskUseCase: UpdateTaskUseCase,
    private readonly deleteTaskUseCase: DeleteTaskUseCase*/
  ) { }

    
  async addTag(req: Request, res: Response) {
      //On valide avec le schema
      const result = validate(req.body, tagCreateSchema)
      if (!result.valid) {
        const errors = result.errors.map((error: ValidationError) => {
          return {
            message: error.message
          }
        })
        return res.status(400).json(errors)
      }
  
      const tag = await this.addTagUseCase.execute(req.body)
      res.status(201).json(tag)
    }

  
  async getTags(req: Request, res: Response) {
    const tasks = await this.getTagsUseCase.execute()
    res.status(200).json(tasks)
  }

  /*
  async getTask(req: Request, res: Response) {
    try {
      const task = await this.getTaskUseCase.execute(req.params.id)
      res.status(200).json(task)
    } catch (error) {
      const httpResponse = convertErrorsToHttpResponse(error)
      res.status(httpResponse.status).json(httpResponse.body)
    }
  }

  async updateTask(req: Request, res: Response) {
    if(req.body.status<0 || req.body.status>2){
      return res.status(400).json({message:"Unvalid value for status"});
    }
    const task = await this.updateTaskUseCase.execute(req.params.id, req.body)
    res.status(200).json(task)
  }

  async deleteTask(req: Request, res: Response) {
    const task = await this.deleteTaskUseCase.execute(req.params.id)
    res.status(200).json(task)
  }*/
}



function convertErrorsToHttpResponse(error: unknown) {
  // https://www.baeldung.com/rest-api-error-handling-best-practices
  if (error instanceof TaskNotFoundError) {
    return notFound({ message: error.message, code: 'tag-not-found', data: { id: error.id } })
  }
  return internal()
}
