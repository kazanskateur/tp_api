export type Task = {
  id: string
  title: string
  description: string
  status: number
  priority: number
  tagId: string
}

export type TaskCreate = Omit<Task, 'id'>

export type TaskUpdate = Task
