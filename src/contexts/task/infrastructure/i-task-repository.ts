import { Tag } from '@prisma/client';
import { Task, TaskCreate, TaskUpdate } from '../domains/types'
export * from '../domains/errors';

export type TaskRaw = {
  id: string
  title: string
  description: string
  status: string
  priority: number
  tagId: string | null
}
export type TaskCreateRaw = {
  id: string
  title: string
  description: string
  status: number
  priority: number
}
export type TaskUpdateRaw = TaskUpdate

export interface ITaskRepository {
  getTasks(): Promise<TaskRaw[]>
  getTasksByTag(idTag: string): Promise<TaskRaw[]>
  getTasksPaginated(nb: number, page: number): Promise<TaskRaw[]>
  addTask(task: TaskCreate): Promise<TaskRaw>
  getTask(id: string): Promise<TaskRaw | null>
  updateTask(book: TaskUpdate): Promise<TaskRaw>
  deleteTask(id: string): Promise<void>
  addTag(idTask: string, idTag: string): Promise<TaskRaw>
}