import { Router } from 'express'
import { TaskController } from './controller'

export function taskRoutes(controller: TaskController): Router {
  const router = Router()
  router.get('/', controller.getTasks.bind(controller))
  router.post('/', controller.addTask.bind(controller))
  router.get('/:id', controller.getTask.bind(controller))
  router.put('/:id', controller.updateTask.bind(controller))
  router.delete('/:id', controller.deleteTask.bind(controller))
  router.get('/get-by-tag/:idTag', controller.getTasksByTag.bind(controller))
  router.get('/get-paginated/:nbByPage/:page', controller.getTasksPaginated.bind(controller))
  router.put('/update-tag/:idTask/:idTag', controller.addTag.bind(controller))
  return router
}
