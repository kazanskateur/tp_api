import { Request, Response } from 'express'
import { GetTasksUseCase, GetTasksByTagUseCase,GetTasksPaginatedUseCase, AddTaskUseCase, GetTaskUseCase, UpdateTaskUseCase, DeleteTaskUseCase, AddTagUseCase } from '../../use-cases'
import { TaskNotFoundError } from '../../domains'
import { internal, notFound } from '../../../../infrastructure'
import { ValidationError, validate } from 'jsonschema'
import { ValidatorResult } from 'jsonschema'

const taskCreateSchema = {
  id: "/Task",
  type: "object",
  properties: {
    title: {
      type: "string"
    },
    description: {
      type: "string"
    },
    status: {
      type: "number"
    },
    priority: {
      type: "number"
    },
  },
  required: ["title", "status", "priority"]
}

export class TaskController {
  constructor(
    private readonly getTasksUseCase: GetTasksUseCase,
    private readonly getTasksByTagUseCase: GetTasksByTagUseCase,
    private readonly getTasksPaginatedUseCase: GetTasksPaginatedUseCase,
    private readonly addTaskUseCase: AddTaskUseCase,
    private readonly getTaskUseCase: GetTaskUseCase,
    private readonly updateTaskUseCase: UpdateTaskUseCase,
    private readonly deleteTaskUseCase: DeleteTaskUseCase,

    private readonly addTagUseCase: AddTagUseCase,
  ) { }

  async getTasks(req: Request, res: Response) {
    const tasks = await this.getTasksUseCase.execute()
    res.status(200).json(tasks)
  }
  async getTasksByTag(req: Request, res: Response) {
    const tasks = await this.getTasksByTagUseCase.execute(req.params["idTag"])
    res.status(200).json(tasks)
  }

  async getTasksPaginated(req: Request, res: Response) {
    //On verifie que c'est un nombre
    var nb = Number(req.params["nbByPage"])
    var page = Number(req.params["page"])
    if(Number.isNaN(nb) || Number.isNaN(page)){
      return res.status(400).json({message:"Unvalid values for your paramters: NaN"});
    }else{
      const tasks = await this.getTasksPaginatedUseCase.execute(nb,page)
      res.status(200).json(tasks)
    }
  }

  async addTask(req: Request, res: Response) {
    //On valide avec le schema
    const result = validate(req.body, taskCreateSchema)
    if (!result.valid) {
      const errors = result.errors.map((error: ValidationError) => {
        return {
          message: error.message
        }
      })
      return res.status(400).json(errors)
    }
    if(req.body.status<0 || req.body.status>2){
      return res.status(400).json({message:"Unvalid value for status"});
    }
    const task = await this.addTaskUseCase.execute(req.body)
    res.status(201).json(task)
  }

  async getTask(req: Request, res: Response) {
    try {
      const task = await this.getTaskUseCase.execute(req.params.id)
      res.status(200).json(task)
    } catch (error) {
      const httpResponse = convertErrorsToHttpResponse(error)
      res.status(httpResponse.status).json(httpResponse.body)
    }
  }

  async updateTask(req: Request, res: Response) {
    if(req.body.status<0 || req.body.status>2){
      return res.status(400).json({message:"Unvalid value for status"});
    }
    const task = await this.updateTaskUseCase.execute(req.params.id, req.body)
    res.status(200).json(task)
  }

  async deleteTask(req: Request, res: Response) {
    const task = await this.deleteTaskUseCase.execute(req.params.id)
    res.status(200).json(task)
  }

  //Dans les params on passe 2 id
  async addTag(req: Request, res: Response) {
    const task = await this.addTagUseCase.execute(req.params["idTask"], req.params["idTag"])
    res.status(200).json(task)
  }
}



function convertErrorsToHttpResponse(error: unknown) {
  // https://www.baeldung.com/rest-api-error-handling-best-practices
  if (error instanceof TaskNotFoundError) {
    return notFound({ message: error.message, code: 'task-not-found', data: { id: error.id } })
  }
  return internal()
}
