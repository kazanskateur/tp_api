import { Router } from 'express'
import { TaskRepository, RelationalDatabase } from '../../../infrastructure/database'
import { taskRoutes } from './task.routes'
import { TaskController } from './controller'
import { GetTasksUseCase,GetTasksByTagUseCase,GetTasksPaginatedUseCase, AddTaskUseCase, GetTaskUseCase, UpdateTaskUseCase, DeleteTaskUseCase, AddTagUseCase } from '../use-cases'

export type TaskExternalDependencies = {
  database: RelationalDatabase
}

export const taskInjector = (externalDependencies: TaskExternalDependencies): Router => {
  const taskRepository = new TaskRepository(externalDependencies.database)

  const getTasksUseCase = new GetTasksUseCase(taskRepository)
  const getTasksByTagUseCase = new GetTasksByTagUseCase(taskRepository)
  const getTasksPaginatedUseCase = new GetTasksPaginatedUseCase(taskRepository)
  const addTaskUseCase = new AddTaskUseCase(taskRepository)
  const getTaskUseCase = new GetTaskUseCase(taskRepository)
  const updateTaskUseCase = new UpdateTaskUseCase(taskRepository)
  const deleteTaskUseCase = new DeleteTaskUseCase(taskRepository)
  const addTagUseCase = new AddTagUseCase(taskRepository)

  const taskController = new TaskController(
    getTasksUseCase,
    getTasksByTagUseCase,
    getTasksPaginatedUseCase,
    addTaskUseCase,
    getTaskUseCase,
    updateTaskUseCase,
    deleteTaskUseCase,
    addTagUseCase
  )

  return taskRoutes(taskController)
}
