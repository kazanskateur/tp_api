import { Task } from '../../domains/types'
import { ITaskRepository, TaskRaw } from '../../infrastructure'

export class GetTasksUseCase {
  constructor(private taskRepository: ITaskRepository) { }

  async execute(): Promise<TaskRaw[]> {
    return await this.taskRepository.getTasks()
  }
}
