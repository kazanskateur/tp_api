import { Task } from '../../domains/types'
import { ITaskRepository, TaskRaw } from '../../infrastructure'

export class GetTasksPaginatedUseCase {
  constructor(private taskRepository: ITaskRepository) { }

  async execute(nb: number, page: number): Promise<TaskRaw[]> {
    return await this.taskRepository.getTasksPaginated(nb,page)
  }
}
