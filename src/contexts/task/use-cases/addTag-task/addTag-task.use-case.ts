import { Task, TaskUpdate } from '../../domains/types'
import { ITaskRepository, TaskRaw } from '../../infrastructure'

export class AddTagUseCase {
  constructor(private taskRepository: ITaskRepository) {}

  async execute(idTask: string, idTag: string): Promise<TaskRaw> {
    return this.taskRepository.addTag(idTask,idTag)
  }
}
