import { Task } from '../../domains/types'
import { ITaskRepository, TaskRaw } from '../../infrastructure'

export class GetTasksByTagUseCase {
  constructor(private taskRepository: ITaskRepository) { }

  async execute(idTag: string): Promise<TaskRaw[]> {
    return await this.taskRepository.getTasksByTag(idTag)
  }
}
