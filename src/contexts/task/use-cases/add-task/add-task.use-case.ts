import { Task, TaskCreate } from '../../domains/types'
import { ITaskRepository, TaskRaw } from '../../infrastructure'

export class AddTaskUseCase {
  constructor(private taskRepository: ITaskRepository) {}

  async execute(task: TaskCreate): Promise<TaskRaw> {
    return this.taskRepository.addTask(task)
  }
}
