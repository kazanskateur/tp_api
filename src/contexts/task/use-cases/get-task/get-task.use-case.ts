import { TaskNotFoundError, Task } from '../../domains'
import { ITaskRepository, TaskRaw } from '../../infrastructure'

export class GetTaskUseCase {
  constructor(private bookRepository: ITaskRepository) { }

  async execute(id: string): Promise<TaskRaw> {
    const task = await this.bookRepository.getTask(id)
    if (!task) {
      throw new TaskNotFoundError(id)
    } else {
      return task
    }
  }
}
