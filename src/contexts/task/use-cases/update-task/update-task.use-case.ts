import { Task, TaskUpdate } from '../../domains/types'
import { ITaskRepository, TaskRaw } from '../../infrastructure'

export class UpdateTaskUseCase {
  constructor(private taskRepository: ITaskRepository) {}

  async execute(id: string, task: Omit<TaskUpdate, 'id'>): Promise<TaskRaw> {
    return this.taskRepository.updateTask({ ...task, id })
  }
}
