import { error } from 'console'
import { TaskCreateRaw, TaskNotFoundError, TaskRaw, ITaskRepository } from '../../../../contexts/task'
import { Task, TaskCreate, TaskUpdate } from '../../../../contexts/task/domains/types'
import { RelationalDatabase } from '../../database'
import { toTaskRaw } from './task.mapper'
import { Prisma } from '@prisma/client'

export class TaskRepository implements ITaskRepository {
  constructor(private readonly database: RelationalDatabase) { }

  async getTasks(): Promise<TaskRaw[]> {
    const tasks = await this.database.client.task.findMany({
      take: 10,
      orderBy: {
          priority: 'asc',
      },
    })
    return tasks.map(toTaskRaw)
  }

  async getTasksByTag(idTag : string): Promise<TaskRaw[]> {
    const tasks = await this.database.client.task.findMany({
      where: { tagId: idTag },
      orderBy: {
          priority: 'asc',
      },
    })
    return tasks.map(toTaskRaw)
  }

  async getTasksPaginated(nb : number, page : number): Promise<TaskRaw[]> {
    const tasks = await this.database.client.task.findMany({
      skip:(page-1)*nb,
      take:nb,
      orderBy: {
          priority: 'asc',
      },
    })
    return tasks.map(toTaskRaw)
  }

  async addTask(task: TaskCreate): Promise<TaskRaw> {
    const newTask = await this.database.client.task.create({ data: task })
    return toTaskRaw(newTask)
  }

  async getTask(id: string): Promise<TaskRaw | null> {
    const task = await this.database.client.task.findUnique({ where: { id } })
    return task ? toTaskRaw(task) : null
  }

  async updateTask(task: TaskUpdate): Promise<TaskRaw> {
    const taskUpdated = await this.database.client.task.update({
      where: { id: task.id },
      data: task
    })
    return toTaskRaw(taskUpdated)
  }

  async deleteTask(id: string): Promise<void> {
    try {
      await this.database.client.task.delete({ where: { id } })
    } catch (error) {
      if (!(error instanceof Prisma.PrismaClientKnownRequestError) || error.code !== 'P2025') {
        throw error
      }
    }
  }

  async addTag(idTask: string, idTag: string): Promise<TaskRaw > {
    const fetchedTag = await this.database.client.tag.findUnique({ where: { id: idTag } })
    if(fetchedTag != null){
      const taskUpdated = await this.database.client.task.update({
        where: { id: idTask },
        data: {
          tagId: fetchedTag.id,
        }
      })
      return toTaskRaw(taskUpdated)
    }
    else{
      throw error(idTag)
    }
  }

}
