import { Task } from "@prisma/client";
import { TaskCreateRaw, TaskRaw } from '../../../../contexts/task'
import { TaskCreate } from "../../../../contexts/task/domains";

export function toTaskRaw(task: Task): TaskRaw {
  var status:string = ""
  switch(task.status) { 
    case 0: { 
      status = "A faire";
      break; 
    } 
    case 1: { 
      status = "En cours";
      break; 
    } 
    case 2: { 
      status = "Fini";
      break; 
    } 
 } 
  return {
    id: task.id,
    title: task.title,
    description: task.description,
    status: status,
    priority: task.priority,
    tagId: task.tagId
  };
}
