import { Tag } from "@prisma/client";
import { TagRaw } from '../../../../contexts/tag'
import { TaskCreate } from "../../../../contexts/task/domains";

export function toTagRaw(tag: Tag): TagRaw {
  return {
    id: tag.id,
    title: tag.title,
  };
}
