import { TaskNotFoundError, TagRaw, ITagRepository } from '../../../../contexts/tag'
import { Tag, TagCreate } from '../../../../contexts/tag/domains/types'
import { RelationalDatabase } from '../../database'
import { toTagRaw } from './tag.mapper'
import { Prisma } from '@prisma/client'

export class TagRepository implements ITagRepository {
  constructor(private readonly database: RelationalDatabase) { }

  async addTag(tag: TagCreate): Promise<TagRaw> {
    const newTask = await this.database.client.tag.create({ data: tag })
    return toTagRaw(newTask)
  }
  async getTags(): Promise<TagRaw[]> {
    const tags = await this.database.client.tag.findMany({
      take: 10,
    })
    return tags.map(toTagRaw)
  }

}
